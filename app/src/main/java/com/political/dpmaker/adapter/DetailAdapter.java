package com.political.dpmaker.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.political.dpmaker.R;
import com.political.dpmaker.model.Model;
import com.squareup.picasso.Picasso;

public class DetailAdapter extends FirebaseRecyclerAdapter<Model,DetailAdapter.ViewHolder> {

    private Context context;

    /**
     * Initialize a {@link RecyclerView.Adapter} that listens to a Firebase query. See
     * {@link FirebaseRecyclerOptions} for configuration options.
     *
     * @param options
     */
    public DetailAdapter(@NonNull FirebaseRecyclerOptions<Model> options,Context context) {
        super(options);

        this.context=context;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

    View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.detail_layout,parent,false);
        return new ViewHolder(view);
    }
    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull Model model) {
        holder.tv_detailname.setText(model.getName());
        Picasso.get().load(model.getImage()).into(holder.iv_detailimage);
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_detailimage;
        private TextView tv_detailname;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_detailimage=itemView.findViewById(R.id.iv_detailimage);
            tv_detailname=itemView.findViewById(R.id.tv_detailname);

        }
    }
}
