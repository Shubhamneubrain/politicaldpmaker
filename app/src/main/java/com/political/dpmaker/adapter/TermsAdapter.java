package com.political.dpmaker.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.political.dpmaker.R;

import java.util.List;

public class TermsAdapter extends RecyclerView.Adapter<TermsAdapter.ViewHolder> {

    private List<String> list;
    private Context context;

    public TermsAdapter(List<String>list,Context context)
    {
        this.list=list;
        this.context=context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_layout,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.tv_text.setText(list.get(position));

        if(position==1)
        {
            holder.tv_text.setTextColor(Color.BLACK);
            holder.tv_text.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));

        }
        else if(position==2)
        {
            holder.tv_text.setTextColor(Color.BLACK);
            holder.tv_text.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        }
        else if(position==3)
        {
            holder.tv_text.setTextColor(Color.BLACK);
            holder.tv_text.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        }
        else if (position==4)
        {
            holder.tv_text.setTextColor(Color.BLACK);
            holder.tv_text.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        }
        else if (position==5)
        {
            holder.tv_text.setTextColor(Color.BLACK);
            holder.tv_text.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        }

        else {

            holder.tv_text.setTextColor(Color.BLACK);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_text;

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            tv_text=itemView.findViewById(R.id.tv_text);
        }
    }
}
