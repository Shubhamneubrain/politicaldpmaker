package com.political.dpmaker.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.political.dpmaker.R;

import java.util.List;

public class AboutUsAdapter extends RecyclerView.Adapter<AboutUsAdapter.ViewHolder> {

 private Context context;
 private List<String>list;

 public AboutUsAdapter(Context context,List<String>list)
 {
     this.context=context;
     this.list=list;
 }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

     View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_layout,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

     holder.tv_text.setText(list.get(position));

     if(position==0)
     {
         holder.tv_text.setTextColor(Color.BLACK);
         holder.tv_text.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
     }
     else
     {
         holder.tv_text.setTextColor(Color.BLACK);
     }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

     private TextView tv_text;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
     tv_text=itemView.findViewById(R.id.tv_text);

        }
    }
}
