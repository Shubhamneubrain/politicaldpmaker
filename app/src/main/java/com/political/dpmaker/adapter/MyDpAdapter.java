package com.political.dpmaker.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.political.dpmaker.R;

import java.util.List;

public class MyDpAdapter extends RecyclerView.Adapter<MyDpAdapter.ViewHolder> {

private Context context;
private List<Integer>a;

public MyDpAdapter(Context context,List<Integer>a)
{
    this.context=context;
    this.a=a;
}

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

    View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.mydp_layout,parent,false);
    return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

    holder.iv_mydp.setImageResource(a.get(position));

    }

    @Override
    public int getItemCount() {
        return a.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

    private ImageView iv_mydp;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_mydp=itemView.findViewById(R.id.iv_mydp);
        }
    }
}
