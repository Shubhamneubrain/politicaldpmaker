package com.political.dpmaker;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.political.dpmaker.databinding.ActivityPhotoBinding;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class PhotoActivity extends AppCompatActivity {

 private ActivityPhotoBinding activityPhotoBinding;
 private static final int   GALLERY_REQUEST=12;
private Uri imageuri=null;
private ProgressDialog progressDialog;

private StorageReference reference;

     @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityPhotoBinding=ActivityPhotoBinding.inflate(getLayoutInflater());
        View view=activityPhotoBinding.getRoot();
        setContentView(view);
    sendDatabaseToFirebase();
    }



    private void sendDatabaseToFirebase()
    {
progressDialog=new ProgressDialog(this);
        List<String> categories = new ArrayList<>();
        categories.add(0,"Select Imag0+0e Type");
        categories.add(1,"JPEG");
        categories.add(2,"PNG");

        ArrayAdapter<String> adapter= new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, categories);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        activityPhotoBinding.spinner.setAdapter(adapter);
        reference= FirebaseStorage.getInstance().getReference();
        activityPhotoBinding.ivPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(intent,GALLERY_REQUEST);
            }
        });


        activityPhotoBinding.btnSendtodatabase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String photoname = activityPhotoBinding.etImagename.getText().toString().trim();


                if (TextUtils.isEmpty(photoname)) {
                    Toast.makeText(PhotoActivity.this, "Please Enter Image Name", Toast.LENGTH_SHORT).show();
                }
                else if (!TextUtils.isEmpty(photoname)&&imageuri!=null){

                    progressDialog.setMessage("Posting to database..");
                    progressDialog.setCanceledOnTouchOutside(false);
                    progressDialog.show();

                    final StorageReference filepath=reference.child("blog_image").child(imageuri.getLastPathSegment());
                    filepath.putFile(imageuri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {


                            filepath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {

                                    Uri downloadurl=uri;

                                    HashMap<String, Object> map = new HashMap<>();
                                    map.put("name", photoname);
                                    map.put("image",downloadurl.toString());
                                    map.put("image type",activityPhotoBinding.spinner.getSelectedItem().toString());

                                    FirebaseDatabase.getInstance().getReference().child("dpimage").push().setValue(map)
                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    Toast.makeText(PhotoActivity.this, "Sucessfully add to database", Toast.LENGTH_SHORT).show();
                                             Intent intent=new Intent(PhotoActivity.this,DetailActivity.class);
                                             startActivity(intent);

                                                    progressDialog.dismiss();
                                                }
                                            }).addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {

                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Toast.makeText(PhotoActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    });

                                }
                            });
                        }
                    });
                }
            }
        });

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==GALLERY_REQUEST&&resultCode==RESULT_OK)
        {
            Uri imageUri=data.getData();
            CropImage.activity(imageUri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(1,1)
                    .start(this);
        }


        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                imageuri = result.getUri();
                activityPhotoBinding.ivPhoto.setImageURI(null);
                activityPhotoBinding.ivPhoto.setImageURI(imageuri);
                activityPhotoBinding.ivPhoto.setBackgroundResource(0);


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }
}
