package com.political.dpmaker.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.political.dpmaker.R;
import com.political.dpmaker.ui.DrawerActivity;

public class HowWorkFragment extends Fragment {

private NavController navController;

private View view;

private Toolbar toolbarhowworks;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         view= inflater.inflate(R.layout.fragment_howwork, container, false);
       setToolbar();
        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navController= Navigation.findNavController(view);

    }

    private void setToolbar()
    {
        toolbarhowworks=view.findViewById(R.id.toolbarhowworks);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbarhowworks); //toolbar id
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("How It Works ");
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbarhowworks.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(getContext(), DrawerActivity.class);
                startActivity(intent);
            }
        });

    }

}
