package com.political.dpmaker.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.political.dpmaker.R;
import com.political.dpmaker.adapter.RecyclerviewAdapter;
import com.political.dpmaker.databinding.FragmentPrivacyBinding;
import com.political.dpmaker.ui.DrawerActivity;

import java.util.ArrayList;
import java.util.List;

public class PrivacyFragment extends Fragment {

private FragmentPrivacyBinding fragmentPrivacyBinding;

private Toolbar toolbarone;

private View view;

private NavController navController;

public PrivacyFragment()
{

}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
      fragmentPrivacyBinding=FragmentPrivacyBinding.inflate(inflater,container,false);
       view=fragmentPrivacyBinding.getRoot();
      setAdapter();
      setToolbar();
      return view;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        fragmentPrivacyBinding=null;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navController= Navigation.findNavController(view);

    }

    private void setToolbar()
    {
        toolbarone=view.findViewById(R.id.toolbarone);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbarone); //toolbar id
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Privacy and Liscences ");
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbarone.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(getContext(), DrawerActivity.class);
                startActivity(intent);

            }
        });
    }

    private void setAdapter()
    {
        List<String> a=new ArrayList<>();
        a.add("Lorem Ipsum is simply dummy text of the printing and typesetting industry." +
                " Lorem Ipsum has been the industry's standard dummy text ever since the 1500s," +
                " when an unknown printer took a galley of type and scrambled it to make a type specimen book." +
                " It has survived not only five centuries, but also the leap into electronic typesetting," +
                " remaining essentially unchanged. It was popularised in the 1960s with the release ");
        a.add(getString(R.string.text));
        a.add("• Section 1.10.33 of \"de Finibus Bonorum et Malorum\", written by Cicero in 45 BC");
        a.add("• Finibus Bonorum et Malorum");
        a.add("• Governing Laws");
        a.add("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt" +
                " ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud" +
                " exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute" +
                " irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.");

        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        DividerItemDecoration dividerItemDecoration=new DividerItemDecoration(requireContext(),DividerItemDecoration.VERTICAL);
        fragmentPrivacyBinding.recyclerview.addItemDecoration(dividerItemDecoration);
        RecyclerviewAdapter adapter = new RecyclerviewAdapter(a, getContext());
        fragmentPrivacyBinding.recyclerview.setAdapter(adapter);
        fragmentPrivacyBinding.recyclerview.setLayoutManager(linearLayoutManager);

    }
}
