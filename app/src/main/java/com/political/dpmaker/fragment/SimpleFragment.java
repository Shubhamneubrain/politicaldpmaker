package com.political.dpmaker.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.political.dpmaker.R;
import com.political.dpmaker.databinding.FragmentSimpledpBinding;


public class SimpleFragment extends Fragment {

private FragmentSimpledpBinding fragmentMydpBinding;

private NavController navController;

private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentMydpBinding=FragmentSimpledpBinding.inflate(inflater,container,false);
        view=fragmentMydpBinding.getRoot();
       setIcontoEditext();
        setCreateMyDp();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        fragmentMydpBinding=null;

    }

    private void setCreateMyDp()
    {
        fragmentMydpBinding.btnCreatedp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                navController.navigate(R.id.action_mysimpleFragment_to_addphotoActivity);

            }
        });

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navController= Navigation.findNavController(view);

    }

    private void setIcontoEditext()
    {
     //   fragmentMydpBinding.etSearch.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_settings, 0, 0, 0);

            }
    }
