package com.political.dpmaker.fragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.political.dpmaker.R;
import com.political.dpmaker.adapter.AboutUsAdapter;
import com.political.dpmaker.databinding.FragmentAboutusBinding;

import java.util.ArrayList;
import java.util.List;

public class AboutUsFragment extends Fragment {

private FragmentAboutusBinding fragmentAboutusBinding;

private AboutUsAdapter aboutUsAdapter;

private View view;

private Toolbar toolbaraboutus;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentAboutusBinding=FragmentAboutusBinding.inflate(inflater,container,false);
        view=fragmentAboutusBinding.getRoot();
        setToolbar();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        fragmentAboutusBinding=null;
    }

    private void setToolbar()
    {
        toolbaraboutus=view.findViewById(R.id.toolbaraboutus);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbaraboutus); //toolbar id
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("About Us");
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbaraboutus.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(getContext(), AboutUsFragment.class);
                startActivity(intent);

            }
        });


        setAdapterForAboutUs();
    }

    private void setAdapterForAboutUs()
    {

        List<String>a=new ArrayList<>();
        a.add("DPlogi provides the best features:");
        a.add("New filters, frames and stickers of the political party of your choice");
        a.add("Support your political figure by making your profile attractive with a well-edited DP");
        a.add("Get the perfect authentic DP for campaigning");
        a.add("Seek the attention of your favourite political party");
        a.add("Free tutorials on how to make your photo into a wonderful DP");
        a.add("Frequent updates and bug-fixing.");
        a.add("No internet required, so no more buffering");
        a.add("Not a paid app, so make your DP for FREE!");

        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        aboutUsAdapter=new AboutUsAdapter(getContext(),a);
        fragmentAboutusBinding.recyclerviewAboutus.setAdapter(aboutUsAdapter);
        fragmentAboutusBinding.recyclerviewAboutus.setLayoutManager(linearLayoutManager);

    }

}
