package com.political.dpmaker.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.political.dpmaker.R;
import com.political.dpmaker.databinding.FragmentProfileBinding;
import com.political.dpmaker.ui.DrawerActivity;

public class ProfileFragment extends Fragment {

private FragmentProfileBinding profileBinding;

private View view;

private NavController navController;

private Toolbar toolbarthree;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        profileBinding=FragmentProfileBinding.inflate(inflater, container, false);
        view=profileBinding.getRoot();
        setToolbar();
        return  view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        profileBinding=null;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navController= Navigation.findNavController(view);

    }

    private void setToolbar()
    {
        toolbarthree=view.findViewById(R.id.toolbarthree);
        TextView toolbar_titles=toolbarthree.findViewById(R.id.toolbar_titles);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbarthree);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbarthree.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(getContext(), DrawerActivity.class);
                startActivity(intent);

            }
        });
    }


}
