package com.political.dpmaker.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.political.dpmaker.R;
import com.political.dpmaker.adapter.MyDpAdapter;
import com.political.dpmaker.databinding.FragmentMydpBinding;
import com.political.dpmaker.ui.DrawerActivity;

import java.util.ArrayList;
import java.util.List;


public class MyDpFragments extends Fragment {

    private FragmentMydpBinding fragmentMydpBinding;

    private MyDpAdapter adapter;

    private View view;

    private Toolbar toolbarfour;

    private NavController navController;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentMydpBinding = FragmentMydpBinding.inflate(inflater, container, false);
         view = fragmentMydpBinding.getRoot();

        setListForMyDp();

        setToolbar();

        return view;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        fragmentMydpBinding = null;
    }

    private void setToolbar()
    {
        toolbarfour=view.findViewById(R.id.toolbarfour);
        final TextView toolbar_titles=toolbarfour.findViewById(R.id.toolbar_titlefour);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbarfour);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbarfour.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(getContext(), DrawerActivity.class);
                startActivity(intent);

            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navController= Navigation.findNavController(view);

    }

    private void setListForMyDp() {
        List<Integer> a = new ArrayList<>();
        a.add(R.drawable.mydpone);
        a.add(R.drawable.mydptwo);
        a.add(R.drawable.mydpthree);
        a.add(R.drawable.mydpone);
        a.add(R.drawable.mydptwo);
        a.add(R.drawable.mydpthree);
        a.add(R.drawable.mydpone);
        a.add(R.drawable.mydptwo);
        a.add(R.drawable.mydpthree);
        a.add(R.drawable.mydpone);
        a.add(R.drawable.mydptwo);
        a.add(R.drawable.mydpthree);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3);
        fragmentMydpBinding.recyclerviewMydp.setLayoutManager(gridLayoutManager);
        adapter = new MyDpAdapter(getContext(), a);
        fragmentMydpBinding.recyclerviewMydp.setAdapter(adapter);
    }
}
