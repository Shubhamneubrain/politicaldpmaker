package com.political.dpmaker.fragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.political.dpmaker.R;
import com.political.dpmaker.adapter.TermsAdapter;
import com.political.dpmaker.databinding.FragmentTermsconditionBinding;
import com.political.dpmaker.ui.DrawerActivity;

import java.util.ArrayList;
import java.util.List;

public class TermsConditionFragment extends Fragment {


private FragmentTermsconditionBinding fragmentTermsconditionBinding;

    private Toolbar toolbartwo;

    private View view;

    private NavController navController;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentTermsconditionBinding=FragmentTermsconditionBinding.inflate(inflater,container,false);
         view=fragmentTermsconditionBinding.getRoot();
        setToolbar();
        return view;

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        fragmentTermsconditionBinding=null;
    }

    private void setToolbar()
    {
        toolbartwo=view.findViewById(R.id.toolbarcool);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbartwo); //toolbar id
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Terms and Condition ");
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbartwo.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(getContext(), DrawerActivity.class);
                startActivity(intent);

            }
        });
        setAdapter();
    }

    private void setAdapter()
    {
        List<String> a=new ArrayList<>();
        a.add("Lorem Ipsum is simply dummy text of the printing and typesetting industry." +
                " Lorem Ipsum has been the industry's standard dummy text ever since the 1500s," +
                " when an unknown printer took a galley of type and scrambled it to make a type specimen book." +
                " It has survived not only five centuries, but also the leap into electronic typesetting," +
                " remaining essentially unchanged. It was popularised in the 1960s with the release ");
        a.add(getString(R.string.text));
        a.add("2.User Conduct and Rules on the Platform");
        a.add("3.Trademark,Copyright and Restriction");
        a.add("4.Jurisdictional issue");
        a.add("5.Governing");

        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(requireContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        DividerItemDecoration dividerItemDecoration=new DividerItemDecoration(requireContext(),DividerItemDecoration.VERTICAL);
        fragmentTermsconditionBinding.recyclerviewTerms.addItemDecoration(dividerItemDecoration);
        TermsAdapter adapter = new TermsAdapter(a, requireContext());
        fragmentTermsconditionBinding.recyclerviewTerms.setAdapter(adapter);
        fragmentTermsconditionBinding.recyclerviewTerms.setLayoutManager(linearLayoutManager);

    }

}
