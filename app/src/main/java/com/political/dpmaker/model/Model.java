package com.political.dpmaker.model;

public class Model {

    private String name,image,spinner;

    public Model()
    {

    }

    public Model(String name, String image,String spinner) {
        this.name = name;
        this.image = image;
        this.spinner=spinner;
    }

    public String getName() {
        return name;
    }

    public String getSpinner() {
        return spinner;
    }

    public String getImage() {
        return image;
    }
}
