package com.political.dpmaker.ui;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.political.dpmaker.R;

public class ReferActivity extends Fragment {

    private Toolbar toolbar_refer;

    private View view;

    private NavController navController;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.activity_refer,container,false);
        getids();
        return view;
    }


    private void getids() {
        toolbar_refer = view.findViewById(R.id.toolbar_refer);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar_refer); //toolbar id
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Refer and Earn");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar_refer.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(getContext(),DrawerActivity.class);
                startActivity(intent);

            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navController= Navigation.findNavController(view);
    }


    }
