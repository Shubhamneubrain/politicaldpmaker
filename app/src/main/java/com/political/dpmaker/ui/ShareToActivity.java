package com.political.dpmaker.ui;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.political.dpmaker.R;

public class ShareToActivity extends Fragment {

private NavController navController;

private Toolbar toolbar_shareto;

private View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         view=inflater.inflate(R.layout.activity_shareto,container,false);
         getids();
        return view;
    }

    private void getids()
    {
        toolbar_shareto=view.findViewById(R.id.toolbar_shareto);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar_shareto); //toolbar id
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Share To");
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar_shareto.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(getContext(),DrawerActivity.class);
                startActivity(intent);

            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navController= Navigation.findNavController(view);
    }
}
