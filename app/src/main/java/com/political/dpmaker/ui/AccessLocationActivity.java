package com.political.dpmaker.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.political.dpmaker.R;
import com.political.dpmaker.databinding.ActivityAccesslocationBinding;

public class AccessLocationActivity extends AppCompatActivity {

private ActivityAccesslocationBinding activityAccesslocationBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityAccesslocationBinding=ActivityAccesslocationBinding.inflate(getLayoutInflater());
        View view=activityAccesslocationBinding.getRoot();
        setContentView(view);
        getids();

    }

    private void getids()
    {
        activityAccesslocationBinding.btnAlertdialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog=new Dialog(AccessLocationActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setContentView(R.layout.dialog_layout);
                dialog.setCancelable(true);
                dialog.show();

            }
        });
    }

}
