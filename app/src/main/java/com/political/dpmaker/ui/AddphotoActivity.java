package com.political.dpmaker.ui;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.political.dpmaker.R;
import com.political.dpmaker.databinding.ActivityAddphotoBinding;

public class AddphotoActivity extends Fragment {

    private View view;

    private ActivityAddphotoBinding activityAddphotoBinding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        activityAddphotoBinding = ActivityAddphotoBinding.inflate(inflater, container, false);
        view = activityAddphotoBinding.getRoot();
        setPermissionForCamera();
        return view;

    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
        activityAddphotoBinding = null;

    }

    private void setPermissionForCamera() {

        activityAddphotoBinding.button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(requireActivity(), new String[]{

                            Manifest.permission.CAMERA

                    }, 100);
                } else {

                    openCamera();
                }

            }
        });
    }

    private void openCamera()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 100);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Bitmap bitmap = (Bitmap) data.getExtras().get("data");
        activityAddphotoBinding.ivAddphoto.setImageBitmap(bitmap);

    }
}
