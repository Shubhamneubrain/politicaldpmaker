package com.political.dpmaker.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.renderscript.ScriptIntrinsicYuvToRGB;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.political.dpmaker.R;
import com.political.dpmaker.databinding.ActivityRegisterBinding;

import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity {

private ActivityRegisterBinding activityRegisterBinding;

private FirebaseAuth auth;

private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityRegisterBinding=ActivityRegisterBinding.inflate(getLayoutInflater());
        View view=activityRegisterBinding.getRoot();
        setContentView(view);

        setFirebaseSignUp();

    }

private void setFirebaseSignUp()
{
    auth=FirebaseAuth.getInstance();

    activityRegisterBinding.btnSignin.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

    final String name=activityRegisterBinding.etRegistername.getText().toString().trim();
    String email=activityRegisterBinding.etRegisteremail.getText().toString().trim();
    String password=activityRegisterBinding.etRegisterPassword.getText().toString().trim();
    String confirmpassword=activityRegisterBinding.etRegisterconfirmpassword.getText().toString().trim();

    if (TextUtils.isEmpty(name)&&TextUtils.isEmpty(email)&&TextUtils.isEmpty(password)&&TextUtils.isEmpty(confirmpassword))
    {
        Toast.makeText(RegisterActivity.this, "Please Enter All Fields", Toast.LENGTH_SHORT).show();
    }

    else if(TextUtils.isEmpty(name))
    {
        Toast.makeText(RegisterActivity.this, "Please Enter Name", Toast.LENGTH_SHORT).show();
    }

    else if(TextUtils.isEmpty(email))
    {
        Toast.makeText(RegisterActivity.this, "Please Enter Email" , Toast.LENGTH_SHORT).show();
    }

    else if (TextUtils.isEmpty(password))
    {
        Toast.makeText(RegisterActivity.this, "Please Enter Password", Toast.LENGTH_SHORT).show();
    }

    else if(TextUtils.isEmpty(confirmpassword))
    {
        Toast.makeText(RegisterActivity.this, "Please Enter Confirm Password", Toast.LENGTH_SHORT).show();
    }
   else if(password.length()<7)
    {
        Toast.makeText(RegisterActivity.this,"Password too short",Toast.LENGTH_SHORT).show();
    }

   else if (!password.matches(confirmpassword))
    {
        Toast.makeText(RegisterActivity.this, "Please Match password and Confirmpassword", Toast.LENGTH_SHORT).show();
    }

   else if(password.equals(confirmpassword))
    {
       progressDialog=new ProgressDialog(RegisterActivity.this);
       progressDialog.setMessage("Registering User");
       progressDialog.setCanceledOnTouchOutside(false);
       progressDialog.show();

        auth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if(task.isSuccessful())
                {

                    FirebaseUser current_user=FirebaseAuth.getInstance().getCurrentUser();
                    String uid=current_user.getUid();

                    FirebaseDatabase firebaseDatabase=FirebaseDatabase.getInstance();

                    DatabaseReference databaseReference=firebaseDatabase.getReference("Users").child(uid);
                    HashMap<String,String>map=new HashMap<>();
                    map.put("name",name);

                    databaseReference.setValue(map).addOnCompleteListener(RegisterActivity.this,new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {

                            if (task.isSuccessful())
                            {
                      Intent intent=new Intent(RegisterActivity.this,LoginActivity.class);
                      startActivity(intent);

                  Toast.makeText(RegisterActivity.this, "Registered Successfully...", Toast.LENGTH_SHORT).show();
                 progressDialog.dismiss();
                            }
                        }
                    });
                }
                else {
                    Toast.makeText(RegisterActivity.this, ""+task.getException(), Toast.LENGTH_SHORT).show();
                  progressDialog.dismiss();
                }
            }
        });
    }
        }
    });
    setListenerForSignInText();
}

    private void setListenerForSignInText() {

        activityRegisterBinding.tvSignintext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(intent);

            }
        });
    }

}
