package com.political.dpmaker.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.political.dpmaker.R;
import com.political.dpmaker.databinding.ActivityLoginBinding;
import com.political.dpmaker.fragment.PrivacyFragment;

public class LoginActivity extends AppCompatActivity {

private ActivityLoginBinding activityLoginBinding;

private FirebaseAuth auth;

private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityLoginBinding=ActivityLoginBinding.inflate(getLayoutInflater());
        View view=activityLoginBinding.getRoot();
        setContentView(view);

        setListenerFirebaseLogin();

    }

    private void setListenerFirebaseLogin()
    {
        auth=FirebaseAuth.getInstance();

        activityLoginBinding.btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

          String email=activityLoginBinding.etEmail.getText().toString().trim();
          String password=activityLoginBinding.etPassword.getText().toString().trim();

          if(TextUtils.isEmpty(email)&&TextUtils.isEmpty(password))
          {
              Toast.makeText(LoginActivity.this, "Please Enter All Fields", Toast.LENGTH_SHORT).show();
          }

          else if(TextUtils.isEmpty(email))
          {
              Toast.makeText(LoginActivity.this, "Please Enter Email", Toast.LENGTH_SHORT).show();
          }

           else if(TextUtils.isEmpty(password))
          {
              Toast.makeText(LoginActivity.this, "Please Enter Password", Toast.LENGTH_SHORT).show();
          }

           else 
          {
              progressDialog=new ProgressDialog(LoginActivity.this);
              progressDialog.setMessage("Login User...");
              progressDialog.setCanceledOnTouchOutside(false);
              progressDialog.show();

           auth.signInWithEmailAndPassword(email,password).addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
               @Override
               public void onComplete(@NonNull Task<AuthResult> task) {
           
                   if(task.isSuccessful())
                   {
                       Toast.makeText(LoginActivity.this, "Login Successfully...", Toast.LENGTH_SHORT).show();
                       Intent intent=new Intent(LoginActivity.this,SelectedlanguageActivity.class);
                       startActivity(intent);

                       progressDialog.dismiss();
                   }
                   else {

                       Toast.makeText(LoginActivity.this, ""+task.getException(), Toast.LENGTH_SHORT).show();

                        progressDialog.dismiss();
                   }
               }
           });
          }
            }
        });
        setListenerForSignInText();

    }

    private void setListenerForSignInText()
    {

        activityLoginBinding.tvSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(intent);

            }
        });

        activityLoginBinding.tvForgottext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

             Intent intent=new Intent(LoginActivity.this,ForgotPasswordActivity.class);
             startActivity(intent);


            }
        });
    }
}
