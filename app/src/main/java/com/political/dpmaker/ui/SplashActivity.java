package com.political.dpmaker.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.political.dpmaker.databinding.ActivitySplashBinding;

import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends AppCompatActivity {

private ActivitySplashBinding activitySplashBinding;

private View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activitySplashBinding=ActivitySplashBinding.inflate(getLayoutInflater());
         view=activitySplashBinding.getRoot();
        setContentView(view);

        settimerforSplashscreen();

    }

    private void settimerforSplashscreen()
    {

        //fullscreensplashcode
      view=getWindow().getDecorView();
      int uioption=View.SYSTEM_UI_FLAG_FULLSCREEN;
      view.setSystemUiVisibility(uioption);

        Timer timer=new Timer();

        timer.schedule(new TimerTask() {
            @Override
            public void run() {

                FirebaseUser firebaseUser= FirebaseAuth.getInstance().getCurrentUser();

                if(firebaseUser!=null)
                {
                    Intent intent=new Intent(SplashActivity.this, SelectedlanguageActivity.class);
                    startActivity(intent);
                    finish();
                }
                else {

                    Intent intent=new Intent(SplashActivity.this,LoginActivity.class);
                    startActivity(intent);
                    finish();

                }
            }
        },3000);
    }
}
