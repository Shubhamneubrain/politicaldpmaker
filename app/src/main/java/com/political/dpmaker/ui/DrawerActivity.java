package com.political.dpmaker.ui;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.os.Bundle;
import android.view.View;

import com.google.android.material.appbar.AppBarLayout;
import com.political.dpmaker.R;
import com.political.dpmaker.databinding.ActivityDrawerBinding;

public class DrawerActivity extends AppCompatActivity {

private ActivityDrawerBinding activityDrawerBinding;

private NavController navController;

private AppBarConfiguration appBarConfiguration;

private Toolbar toolbar;

private AppBarLayout appbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityDrawerBinding=ActivityDrawerBinding.inflate(getLayoutInflater());
        View view=activityDrawerBinding.getRoot();
        setContentView(view);

        getids();

    }

    private void getids()
    {
        toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        appbar=findViewById(R.id.appbar);

navController= Navigation.findNavController(this,R.id.nav_graph);

appBarConfiguration=new AppBarConfiguration.Builder(R.id.privacyFragment,R.id.myDpFragments,R.id.mysimpleFragment,R.id.howWorkFragment)
        .setDrawerLayout(activityDrawerBinding.drawerlayout).build();

        NavigationUI.setupActionBarWithNavController(this,navController,appBarConfiguration);
        NavigationUI.setupWithNavController(activityDrawerBinding.drawer,navController);

      navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
          @Override
          public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {

              if(destination.getId()==R.id.mysimpleFragment)
              {
                  toolbar.setVisibility(View.VISIBLE);
                  appbar.setVisibility(View.VISIBLE);
              }
              else {
                  toolbar.setVisibility(View.GONE);
                  appbar.setVisibility(View.GONE);
              }
          }
      });
    }

    public void onBackPressed(){
        if (activityDrawerBinding.drawerlayout.isDrawerOpen(GravityCompat.START)) {
            activityDrawerBinding.drawerlayout.closeDrawer(GravityCompat.START);
        }
        else {

            super.onBackPressed();

        }
    }
    @Override
    public boolean onSupportNavigateUp() {

        return NavigationUI.navigateUp(navController,appBarConfiguration)
                || super.onSupportNavigateUp();
    }
}
