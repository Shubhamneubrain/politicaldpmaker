package com.political.dpmaker.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.political.dpmaker.R;
import com.political.dpmaker.databinding.ActivitySelectedlanguageBinding;

public class SelectedlanguageActivity extends AppCompatActivity {

private ActivitySelectedlanguageBinding activitySelectedlanguageBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activitySelectedlanguageBinding=ActivitySelectedlanguageBinding.inflate(getLayoutInflater());
        View view=activitySelectedlanguageBinding.getRoot();
        setContentView(view);

        setListenerOnLanguageBtn();

    }

    private void setListenerOnLanguageBtn()
    {
        activitySelectedlanguageBinding.btnSelectedlanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(SelectedlanguageActivity.this,DrawerActivity.class);
                startActivity(intent);
            }

        });
    }

}
