package com.political.dpmaker.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.political.dpmaker.R;
import com.political.dpmaker.databinding.ActivityForgotpasswordBinding;

public class ForgotPasswordActivity extends AppCompatActivity {

private ActivityForgotpasswordBinding forgotpasswordBinding;

private FirebaseAuth auth;

private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        forgotpasswordBinding = ActivityForgotpasswordBinding.inflate(getLayoutInflater());
        View view = forgotpasswordBinding.getRoot();
        setContentView(view);

        setFirebaseForgotPasswordCode();

    }

    private void setFirebaseForgotPasswordCode()
    {
        auth=FirebaseAuth.getInstance();
        forgotpasswordBinding.btnSendemail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

         String email=forgotpasswordBinding.etForgotemail.getText().toString().trim();

         if(TextUtils.isEmpty(email))
         {
             Toast.makeText(ForgotPasswordActivity.this, "Please Enter Email ", Toast.LENGTH_SHORT).show();
         }
       else {

         progressDialog=new ProgressDialog(ForgotPasswordActivity.this);
         progressDialog.setMessage("Send reset Password Email");
         progressDialog.show();

         auth.sendPasswordResetEmail(email).addOnCompleteListener(ForgotPasswordActivity.this, new OnCompleteListener<Void>() {
             @Override
             public void onComplete(@NonNull Task<Void> task) {

                 if(task.isSuccessful())
                 {
                     Toast.makeText(ForgotPasswordActivity.this, "Send reset Password Email", Toast.LENGTH_SHORT).show();
                     progressDialog.dismiss();
                 }
                 else {
                     Toast.makeText(ForgotPasswordActivity.this, ""+task.getException(), Toast.LENGTH_SHORT).show();
                     progressDialog.dismiss();

                 }
             }
         });
         }
            }
        });
    }
}
