package com.political.dpmaker;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.FirebaseDatabase;
import com.political.dpmaker.adapter.DetailAdapter;
import com.political.dpmaker.model.Model;

public class DetailActivity extends AppCompatActivity {


private RecyclerView recyclerview_detail;

private DetailAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        recyclerview_detail=findViewById(R.id.recyclerview_detail);
        GridLayoutManager linearLayoutManager=new GridLayoutManager(this,2);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerview_detail.setLayoutManager(linearLayoutManager);

        FirebaseRecyclerOptions<Model> options =
                new FirebaseRecyclerOptions.Builder<Model>()
                        .setQuery(FirebaseDatabase.getInstance().getReference().child("dpimage"), Model.class)
                        .build();


        adapter=new DetailAdapter(options,DetailActivity.this);
        recyclerview_detail.setAdapter(adapter);
    }
    @Override
    protected void onStart() {
        super.onStart();

        adapter.startListening();

    }

    @Override
    protected void onStop() {
        super.onStop();

        adapter.stopListening();

    }
}